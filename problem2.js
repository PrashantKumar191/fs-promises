const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

function readData(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, { encoding: "utf-8" }, (err, data) => {
      if (err) {
        reject(err);
      } else {
        console.log(`${path.basename(filePath)} read sucessfully \n`);
        resolve(data);
      }
    });
  });
}

function writeData(filePath, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(`Data in ${path.basename(filePath)} written sucessfully \n`);
      }
    });
  });
}

function appendData(filePath, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(filePath, "\n" + data, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(`Data in ${path.basename(filePath)} appended sucessfully \n`);
      }
    });
  });
}

function deleteAllFile(patharray) {
  return new Promise((resolve, reject) => {
    for (let index = 0; index < patharray.length; index++) {
      if (patharray[index]) {
        fs.unlink(`${__dirname}/${patharray[index]}`, (err) => {
          if (err) {
            reject(err);
          } else {
            console.log(`Deleted file ${path.basename(patharray[index])} \n`);
            if (index === patharray.length - 1) {
              resolve("All files deleted sucessfully");
            }
          }
        });
      }
    }
  });
}

function read2Files(filepath1, filepath2) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath1, { encoding: "utf8" }, (err, data1) => {
      if (err) {
        reject(err);
      } else {
        fs.readFile(filepath2, { encoding: "utf8" }, (err, data2) => {
          if (err) {
            reject(err);
          } else {
            let combinedData = data1 + "." + data2;
            let dataArray = combinedData.split(".");
            let sortedData = dataArray.sort();
            sortedData = sortedData.join(".");
            let combinedlipsumPath = path.resolve(
              `${__dirname}`,
              "combinedLipsum.txt"
            );
            resolve(combinedData);
          }
        });
      }
    });
  });
}

const inputData = function (lipsumPath) {
  let upperLipsumPath = path.resolve(__dirname, "upperLipsum.txt");
  let lowerLipsumPath = path.resolve(__dirname, "lowerLipsum.txt");
  let combinedLipsumPath = path.resolve(__dirname, "combinedData.txt");
  let filenamePath = path.resolve(__dirname, "filename.txt");
  readData(lipsumPath)
    .then((data) => {
      let upperData = data.toUpperCase();
      return writeData(upperLipsumPath, upperData);
    })
    .then((data) => {
      console.log(data);
      return writeData(filenamePath, "upperLipsum.txt");
    })
    .then((data) => {
      return readData(upperLipsumPath);
    })
    .then((data) => {
      let lowerData = data.toLowerCase();
      return writeData(lowerLipsumPath, lowerData);
    })
    .then((data) => {
      console.log(data);
      return appendData(filenamePath, "lowerLipsum.txt");
    })
    .then((data) => {
      console.log(data);
      return read2Files(upperLipsumPath, lowerLipsumPath);
    })
    .then((data) => {
      return writeData(combinedLipsumPath, data);
    })
    .then((data) => {
      console.log(data);
      return appendData(filenamePath, "combinedData.txt");
    })
    .then((data) => {
      console.log(data);
      return readData(filenamePath);
    })
    .then((data) => {
      fileNameArray = data.split("\n");
      return deleteAllFile(fileNameArray);
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports.inputData = inputData;
