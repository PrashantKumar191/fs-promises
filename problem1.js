const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

function createDir(path) {
  return new Promise((resolve, reject) => {
    fs.access(path, function (err) {
      if (err) {
        console.log("Creating directory");
        fs.mkdir(path, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve("Directory created sucessfully");
          }
        });
      } else {
        resolve("Directory already exists");
      }
    });
  });
}

function createFile(fileNameArray) {
  return new Promise((resolve, reject) => {
    for (let index = 0; index < fileNameArray.length; index++) {
      fs.writeFile(fileNameArray[index], "hello World", (err) => {
        if (err) {
          reject(err);
        } else {
          console.log(`created file ${path.basename(fileNameArray[index])}`);
          if (index === fileNameArray.length - 1) {
            resolve("all files created sucessfully");
          }
        }
      });
    }
  });
}

function deleteFiles(fileNameArray) {
  return new Promise((resolve, reject) => {
    for (let index = 0; index < fileNameArray.length; index++) {
      fs.unlink(fileNameArray[index], (err) => {
        if (err) {
          reject(err);
        } else {
          if (index === fileNameArray.length - 1) {
            resolve("all files deleted sucessfully");
          }
        }
      });
    }
  });
}

function inputData(dirPath, fileNameArray) {
  createDir(dirPath)
    .then((data) => {
      console.log(data);
      return createFile(fileNameArray);
    })
    .then((data) => {
      console.log(data);
      return deleteFiles(fileNameArray);
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports.inputData = inputData;
