const fs = require("fs");
const path = require("path");
const problem2 = require('../problem2');

lipsumPath = path.resolve(__dirname,'../lipsum.txt');
problem2.inputData(lipsumPath);